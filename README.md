## Frontend code test jordi avella - Dexma - svelte version

### Stack:
- svelte
- tailwindcss
### testing
- jest


## Usage:

First of all, install the dependencies with

 `npm install`

 Then, for development environment run

 `npm run dev`

 To build for production, run:

 `npm run build`

 To launch a local server, run:

 `npm start`  (or `npm run start`)

See package.json for more commands
