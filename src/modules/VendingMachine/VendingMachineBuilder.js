import { get, writable, derived } from "svelte/store"

export default function createVendingMachine(productStockInitial, coinsStockInitial) {
    const productStockStore = writable(productStockInitial)
    const coinsStockStore = writable(coinsStockInitial)
    const insertedCoinsStore = writable([])
    const totalInsertedStore = derived(
        insertedCoinsStore,
        values => values.reduce((acc, coinVal) => acc + coinVal, 0)
    )

    const vendingmachine = new VendingMachine(
        productStockStore,
        coinsStockStore,
        insertedCoinsStore,
        totalInsertedStore,
    )

    return {
        vendingmachine,

        productStockStore,
        coinsStockStore,
        insertedCoinsStore,
        totalInsertedStore,
    }
}

class VendingMachine {
    constructor(productStockStore, coinsStockStore, insertedCoinsStore, totalInsertedStore) {
        this.productStockStore = productStockStore
        this.coinsStockStore = coinsStockStore
        this.insertedCoinsStore = insertedCoinsStore
        this.totalInsertedStore = totalInsertedStore
    }

    getTotalInserted() {
        return get(this.totalInsertedStore)
    }

    findProductById(productId) {
        return get(this.productStockStore).find(p => p.id === productId)
    }

    insertCoin(coinValue) {
        this.insertedCoinsStore.update(currentInsertedCoins => [...currentInsertedCoins, coinValue])
        return this.getTotalInserted()
    }

    selectProduct(productId) {
        const product = this.findProductById(productId)
        if (!this._canSelectProduct(product)) return

        const currentInsertedCoins = get(this.insertedCoinsStore)
        const totalInserted = this.getTotalInserted()
        const amountToRefund = totalInserted - product.cost        
        this._takeInsertedCoins()

        try {
            this._returnChange(amountToRefund)
        } catch (error) {
            this._rollbackInsertedCoins(currentInsertedCoins)
            throw error
        }
        this._decreaseProductStock(productId)
        return product
    }

    refundCoins() {
        const refundedCoins = get(this.insertedCoinsStore)
        this.insertedCoinsStore.set([])
        return refundedCoins
    }

    refillStock(productId, quantity) {
        let finalStockOfThisProduct = 0
        this.productStockStore.update(productsStock => {
            const idx = productsStock.findIndex((p) => p.id === productId)
            productsStock[idx].stock += quantity
            finalStockOfThisProduct = productsStock[idx].stock
            return productsStock
        })
        return finalStockOfThisProduct
    }

    refillCoins(coinValue, quantity) {
        let finalStockOfThisCoin = 0
        this.coinsStockStore.update(coinsStock => {
            const idx = coinsStock.findIndex((c) => c.value === coinValue)
            coinsStock[idx].stock += quantity
            finalStockOfThisCoin = coinsStock[idx].stock
            return coinsStock
        })
        return finalStockOfThisCoin
    }

    _canSelectProduct(product) {
        if (!product) {
            throw new Error("Unknown selected product")
        }
    
        const totalInserted = this.getTotalInserted()
        if (totalInserted < product.cost) {
            throw new Error("Insufficient amount")
        }

        if (product.stock <= 0) {
            throw new Error("Not enough stock")
        }

        return true
    }

    _decreaseProductStock(productId, quantity=1) {
        this.productStockStore.update(products => {
            const productIdx = products.findIndex((p) => p.id === productId)
            products[productIdx].stock -= quantity
            return products
        })
    }

    _takeInsertedCoins() {
        this.insertedCoinsStore.update(insertedCoinsValues => {
            this.coinsStockStore.update(coinsStock => {
                insertedCoinsValues.forEach(insertedCoinValue => {
                    const idx = coinsStock.findIndex(c => c.value === insertedCoinValue)
                    coinsStock[idx].stock++
                })
                return coinsStock
            })
            return []
        })
    }

    _rollbackInsertedCoins(coinCombination) {
        this._reduceCoinsStock(coinCombination)
        this.insertedCoinsStore.update(insertedCoins => [...insertedCoins, ...coinCombination])
    }

    _getAmountIntoCoins(amount) {
        let combination = []

        const orderedCoinStock = [...get(this.coinsStockStore)].sort(
            (a, b) => b.value - a.value
        )
        for (const coin of orderedCoinStock) {
            const coinsToReturn = Math.floor(amount / coin.value)
            const coinsReturned = Math.min(coinsToReturn, coin.stock)
            if (coinsReturned > 0) {
                amount -= coinsReturned * coin.value
                combination = [
                    ...combination,
                    ...new Array(coinsReturned).fill(coin.value),
                ]
            }
        }
        if (amount) return false
        return combination
    }

    _reduceCoinsStock(coinCombination) {
        this.coinsStockStore.update(coinsStock => {
            coinCombination.forEach((coinValue) => {
                const coinIdx = coinsStock.findIndex((c) => c.value === coinValue)
                coinsStock[coinIdx].stock--
            })
            return coinsStock
        })
    }

    _returnChange(amountToRefund) {
        const coinCombination = this._getAmountIntoCoins(amountToRefund)
        if (!coinCombination) {
            throw new Error("No return change combination")
        }
        this._rollbackInsertedCoins(coinCombination)
        return coinCombination
    }
}
