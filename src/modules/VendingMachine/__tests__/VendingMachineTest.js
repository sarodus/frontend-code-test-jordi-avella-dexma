import { get } from "svelte/store";

import createVendingMachine from '../VendingMachineBuilder'
import { POSSIBLE_COINS } from '../constants'

let productStockStore;
let coinsStockStore;
let insertedCoinsStore;
let vendingmachine;
let totalInsertedStore;

describe('With a vending machine', () => {
  beforeEach(() => {
    const productStock = [
      {id: 1, cost: 55, display: "Coke", stock: 3},
      {id: 2, cost: 55, display: "Light Coke", stock: 0},
    ];
    const coinsStock = POSSIBLE_COINS.map(
      (value) => ({value, stock: 2})
    );

    const vendingMachineBuild = createVendingMachine([...productStock], [...coinsStock]);
    productStockStore = vendingMachineBuild.productStockStore;
    coinsStockStore = vendingMachineBuild.coinsStockStore;
    insertedCoinsStore = vendingMachineBuild.insertedCoinsStore;
    vendingmachine = vendingMachineBuild.vendingmachine;
    totalInsertedStore = vendingMachineBuild.totalInsertedStore;
  })

  test('Add multiple coins are reflected correctly', () => {
    vendingmachine.insertCoin(20)
    vendingmachine.insertCoin(50)

    expect(get(insertedCoinsStore)).toEqual([20, 50])
    expect(get(totalInsertedStore)).toEqual(70)
  })

  describe('Select product', () => {
    test('Correctly insert coins and have stock, return the product', () => {
      vendingmachine.insertCoin(100)
      const product = vendingmachine.selectProduct(1)

      expect(product.display).toEqual("Coke")
    })

    test('Without inserting coins raises exception', () => {
      const callSelectProduct = () => vendingmachine.selectProduct(1)

      expect(callSelectProduct).toThrow(Error)
    })

    test('Product without stock raises exception', () => {
      const callSelectProduct = () => vendingmachine.selectProduct(2)

      expect(callSelectProduct).toThrow(Error)

    })

    // The machine have 2 coins of each type
    describe('Change is returned correctly', () => {
      test('125 = 100+20+5', () => {
        vendingmachine.insertCoin(200)
        vendingmachine.selectProduct(1)

        expect(get(insertedCoinsStore)).toEqual([100, 20, 20, 5])
      })

      test('Raise exception when no cannot return change', () => {
        vendingmachine.insertCoin(200)
        vendingmachine.selectProduct(1)
        // now, there is no sufficient change for the next purchase
        const callReturnchange = () => vendingmachine.selectProduct(1)
        expect(callReturnchange).toThrow(Error)
      })
    })
  })

  describe('When refund coins', () => {

    test('Refund nothing when nothing inserted', () => {
      const coinsRefunded = vendingmachine.refundCoins()
      expect(coinsRefunded).toEqual([])
    })

    test('Refund inserted coins', () => {
      vendingmachine.insertCoin(10)
      vendingmachine.insertCoin(20)

      const coinsRefunded = vendingmachine.refundCoins()
      expect(coinsRefunded).toEqual([10, 20])
    })

  })

  describe('When refill stocks', () => {
    test('Refill products', () => {
      const total = vendingmachine.refillStock(1, 5)

      expect(get(productStockStore)[0].stock).toEqual(total)
    })

    test('Refill coins', () => {
      const total = vendingmachine.refillCoins(10, 5)

      expect(get(coinsStockStore)[1].stock).toEqual(total)
    })

  })

})
