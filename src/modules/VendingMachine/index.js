import VendingMachineImplementation from './VendingMachineImplementation.svelte'
import { POSSIBLE_COINS } from './constants'

export { POSSIBLE_COINS }
export default VendingMachineImplementation
